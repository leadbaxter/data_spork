require 'data_spork/version'

module DataSpork
  class Importer
    VERBOSE = false
    VERBOSE_IO_OPTIONS = ENV['VERBOSE'].eql?('true')
    ENCODE_VALUES = false # skip encoding until a need is found
    SANITIZE_VALUES = false # skip sanitizing until a need is found
  end
end

require 'data_spork/importer'