require 'csv'

module DataSpork
  class BaseReader
    attr_reader :owner
    delegate :options, :input_pathname, :to => :owner

    def initialize(owner)
      @owner = owner
    end
  end

  class CSV_Reader < BaseReader
    def each(&block)
      CSV.foreach(input_pathname) do |row|
        block.call row
      end
    end
  end

  class XLSX_Reader < BaseReader
    def each(&block)
      excel = SimpleXlsxReader.open(input_pathname)
      print_error "Excel file was opened and sheet name was found: #{excel.sheets.first.name}"
      excel.sheets.first.rows.each do |row|
        block.call row
      end
    end
  end
end
