require 'google_drive'

module DataSpork
  class Importer::GoogleSpreadsheet < Importer

    def init_options(options)
      super
      unless @options[:google].present?
        @options[:google] = {
          user: ENV['google_user'],
          password: ENV['google_pwd']
        }
      end
    end

    def reader
      if google?
        Reader.new(self)
      else
        super
      end
    end

    def google?
      [ :google, :drive ].include?(input_type)
    end

    def google
      options[:google]
    end

    def google_user
      google[:user]
    end

    def google_password
      google[:password]
    end

    def spreadsheet_title
      google[:spreadsheet_title]
    end

    def worksheet_title
      google[:worksheet_title]
    end

    class Reader < DataSpork::BaseReader
      delegate :print, :print_error, :to => :owner
      delegate :google_user, :google_password, :spreadsheet_title, :worksheet_title, :to => :owner

      def each(&block)
        google = GoogleDrive.login(google_user, google_password)
        print_error "GoogleDrive #{google} login for #{google_user} #{google.present? ? 'succeeded' : 'failed'}."
        print_error "GoogleDrive opening spreadsheet: #{spreadsheet_title}, worksheet: #{worksheet_title}."
        spreadsheet = google.spreadsheet_by_title(spreadsheet_title)
        sheet = spreadsheet.worksheet_by_title(worksheet_title)
        print_error "GoogleDrive was opened and sheet name was found: #{sheet.title}"
        sheet.rows.each do |row|
          block.call(row.collect {|value| value })
        end
      end
    end

  end
end
