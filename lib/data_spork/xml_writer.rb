class DataSpork::XmlWriter < DataSpork::BaseWriter

  # Output the XML document preface.
  def start_document
    print '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>'
    start_schema
  end

  # Output the schema with the root tag.
  def start_schema
    print begin_tag(build_schema_tag)
  end

  # Output the schema with the root tag.
  def build_schema_tag
    %Q(#{root_tag} #{schema_uri} effective_date="#{effective_date}")
  end

  def schema_uri
    %q(xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance")
  end

  # Close out the XML document.
  def finish
    print end_tag root_tag if started?
  end

  def begin_put_row
    print "#{indent}#{begin_row}"
  end

  def end_put_row
    print "#{indent}#{end_row}"
  end

  # Output the value enclosing it with the specified XML tag.
  # @param :tag the xml tag name
  # @param :value the value to output
  def put_tag(tag, value)
    print "#{indent 2}#{begin_tag tag}#{value || ''}#{end_tag tag}"
  end

  # Output the current column enclosing it with XML tags.
  def put_column
    print "#{indent 2}#{begin_tag}#{col_value}#{end_tag}"
  end

  # Answer a begin tag using the current header, or else override it with the sender's tag.
  def begin_tag(h = nil)
    "<#{h || header}>"
  end

  # Answer the end tag using the current header, or else override it with the sender's tag.
  def end_tag(h = nil)
    "</#{h || header}>"
  end

  # Answer the tag defining a row.
  def begin_row
    begin_tag(row_tag)
  end

  # Answer the closing tag for the current row.
  def end_row
    end_tag(row_tag)
  end

  # Answer a string prefix for the specified indent level.
  # @param :level specifies the indent level; default is 1.
  def indent(level = 1)
    "\t" * level
  end
end