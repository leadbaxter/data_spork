require 'data_spork/base_reader'
require 'data_spork/base_writer'
require 'data_spork/xml_writer'
require 'data_spork/json_writer'
require 'data_spork/google_spreadsheet'

module DataSpork

  class Importer
    attr_reader :headers, :input_type, :options, :writers
    attr_reader :row_num, :row, :col_map
    attr_reader :root_tag, :row_tag, :col_tags
    attr_accessor :col_num, :setup_state, :blank_row
    attr_accessor :effective_date

    # Entry point to convert the input document and output it to the selected format(s).
    #
    # @param :input_type symbol indicating whether to output :xlsx, :csv, or :json
    # @param :options hash with options to control the behavior of the conversion
    def self.convert(input_type, options = nil)
      self.new(input_type, options).convert
    end

    # Constructor
    def initialize(input_type, options = nil)
      @input_type = input_type
      init_options options
      init_writers
    end

    def init_options(options)
      @options = { source_path: '.' }.merge(options ||= {})
      @options[:output_path] = @options[:source_path] if @options[:output_path].nil? and @options[:output_file]
    end

    def init_writers
      @writers = [ ]
      add_writers
    end

    def add_writers
      writers << XmlWriter.new(self)
    end

    def effective_date_pattern
      /^[Ee]ffective [Dd]ate+/
    end

    def print(str)
      puts str
    end

    def print_error(str)
      $stderr.puts str
    end

    def write(msg)
      writers.each {|writer| writer.send msg }
    end

    def source_name
      base = DEFAULT_INPUT_NAME
      modifier = ''
      "#{base}#{modifier}.#{input_type}"
    end

    def input_pathname
      Pathname(options[:source_path]).join(source_name).to_s
    end

    def output_pathname
      Pathname(options[:output_path]).join('output')
    end

    def output_filename
      p = output_pathname.join(options[:output_file])
      p.sub_ext "#{file_modifier}#{p.extname}"
    end

    def file_modifier
      ''
    end

    def reader
      if csv?
        CSV_Reader.new(self)
      elsif xlsx?
        XLSX_Reader.new(self)
      end
    end

    def each(&block)
      reader.each &block
    end

    def csv?
      input_type == :csv
    end

    def xlsx?
      [ :xlsx, :xls ].include? input_type
    end

    # Drives the conversion of the CSV input file to XML formatted output.
    # @param :path_to_csv string path name of the CSV input file
    def convert
      start
      each do |row|
        append row
      end
      finish
    end

    def start
      if VERBOSE_IO_OPTIONS
        print_error "options: #{options}"
        print_error "input_pathname: #{input_pathname}, exists: #{File.exist?(input_pathname)}"
        print_error "output_filename: #{output_filename}" if options[:output_file]
      end
      if options[:output_file]
        output_pathname.mkpath
        $stdout = File.open("#{output_filename}", 'w')
      end
      @row_num = 0
      @headers = []
      self.setup_state = :setup_writers
    end

    def finish
      write :finish
    end

    # Appends the specified row to the output.
    # @param :row Array of values parsed from the CSV input.
    def append(row)
      @row = row
      @row_num += 1
      sanitize
      output
    end

    # Sanitize the current row of data. This is done in place, so not worried about a return value.
    def sanitize
      self.col_num = 0
      self.blank_row = true
      row.collect! do |utf_8|
        value = (ENCODE_VALUES ? "#{utf_8}".encode('iso-8859-1', xml: :text) : utf_8)
        self.blank_row = false if blank_row and !value.blank?
        sanitize_value(value) if headers? and SANITIZE_VALUES
        substitute_value(value).tap do
          self.col_num += 1
        end
      end
    end

    # Substitute field-specific values based on their position in the row.
    # The returned value is substituted for the passed value.
    # This method does not process columns that are not included in the output.
    #
    # Subclasses should not override this method, but should override #get_substitute_value instead.
    #
    # @param :value the value to be substituted
    def substitute_value(value)
      if headers? and output_column?
        get_substitute_value(value)
      else
        value
      end
    end

    # Overridden by subclasses to substitute field-specific values based on their position in the row.
    # The returned value is substituted for the passed value.
    # This method expects only columns that are included in the output.
    #
    # @param :value the value to be substituted
    def get_substitute_value(value)
      value
    end

    # Sanitize field-specific values based on their position in the row.
    # The values must be modified in place, so there is no need to return a value.
    # This method does not sanitize columns that are not included in the output.
    #
    # @param :value the value to be sanitized
    def sanitize_value(value)
      if headers? and output_column?
        case header
          when nil?
            0
        end
      end
    end

    # Answer true if the headers are already determined.
    def headers?
      !headers.empty?
    end

    # Output the current row of data, which were parsed from the CSV input.
    def output
      unless reject?.tap { |r| print "rejected #{row_num}: #{row}" if r and VERBOSE }
        if headers.empty?
          send setup_state
        else
          put_row #if location_filter?
        end
      end
    end

    # Answer true if rules dictate the current row should be discarded from processing.
    def reject?
      headers? and blank_row
    end

    # Answer true if the first_col value is the effective date header, and clip the effective date value.
    def clip_effective_date?(first_col)
      if first_col.match(effective_date_pattern)
        self.effective_date = "#{row[1]}".strip
        true
      else
        false
      end
    end

    # Initializes the xml document and transfers setup_state to :setup
    def setup_writers
      write :start
      self.setup_state = :setup
      send setup_state  # automatically transition to next state
    end

    # Initializes the headers on the first row and optionally outputs them when VERBOSE=true.
    def setup
      row.each do |col|
        headers << col_map[col]
      end
      print "headers: #{row_num}: #{headers}" if VERBOSE
    end

    # Answer the value for the current column of data, or for the specified index.
    def col_value(index = nil)
      row[index || col_num]
    end

    # Answer the header for the current column of data, or for the specified index.
    def header(index = nil)
      headers[index || col_num]
    end

    # Answer true when the current column should be included in the output.
    def output_column?
      col_tags.include? header
    end

    def on_begin_row
      write :begin_put_row
    end

    def on_output_column
      write :put_column
    end

    def on_end_row
      write :end_put_row
    end

    # Output the current row, one column at a time.
    def put_row
      on_begin_row
      row.each_index do |index|
        self.col_num = index
        if output_column?
          on_output_column
        end
      end
      on_end_row
    end

  end
end
