module DataSpork
  class BaseWriter
    attr_accessor :started
    attr_reader :owner

    delegate :print, :print_error, :to => :owner
    delegate :effective_date, :col_tags, :root_tag, :row_tag, :header, :col_value, :to => :owner

    def initialize(owner)
      @owner = owner
      @started = false
    end

    def started?
      started
    end

    # Start the document output - subclasses usually override :start_document instead.
    def start
      start_document
      self.started = true
    end

    def start_document
      # override this
    end

    def begin_put_row
      # override this
    end

    def put_column
      # override this
    end

    def end_put_row
      # override this
    end

    # Close out the document.
    def finish
      # override this
    end

  end
end
