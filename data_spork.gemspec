# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'data_spork/version'

Gem::Specification.new do |spec|
  spec.name          = 'data_spork'
  spec.version       = DataSpork::VERSION
  spec.authors       = ['Brian Jackson']
  spec.email         = ['bjackson@leadbaxter.com']
  spec.description   = %q{Importer of CSV and Spreadsheet data.}
  spec.summary       = %q{Import CSV, Excel [.XLS, .XLSX] and Google Drive Spreadsheets. Output to XML or JSON (more formats to come!)}
  spec.homepage      = 'http://bitbucket.org/leadbaxter/data_spork'
  spec.license       = 'MIT'

  spec.files         = Dir['lib/**/*']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = %w(lib)

  spec.add_runtime_dependency 'rails', '~> 3.0'
  spec.add_runtime_dependency 'simple_xlsx_reader', '~> 0.9'
  spec.add_runtime_dependency 'google_drive', '~> 0.3'

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake', '~> 10.1'
end
